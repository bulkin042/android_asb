From 633c8edc77aeb22a8bccaf118ca155a606407096 Mon Sep 17 00:00:00 2001
From: Jouni Malinen <jouni@qca.qualcomm.com>
Date: Tue, 7 Apr 2015 11:32:11 +0300
Subject: [PATCH 04/17] P2P: Validate SSID element length before copying it

This fixes a possible memcpy overflow for P2P dev->oper_ssid in
p2p_add_device(). The length provided by the peer device (0..255 bytes)
was used without proper bounds checking and that could have resulted in
arbitrary data of up to 223 bytes being written beyond the end of the
dev->oper_ssid[] array (of which about 150 bytes would be beyond the
heap allocation) when processing a corrupted management frame for P2P
peer discovery purposes.

This could result in corrupted state in heap, unexpected program
behavior due to corrupted P2P peer device information, denial of service
due to process crash, exposure of memory contents during GO Negotiation,
and potentially arbitrary code execution.

Thanks to Google security team for reporting this issue and smart
hardware research group of Alibaba security team for discovering it.

Signed-off-by: Jouni Malinen <jouni@qca.qualcomm.com>

(cherry picked from commit fdb708a37d8f7f1483e3cd4e8ded974f53fedace)

Change-Id: Ibc53ff533f78cfcd7c51fbb5d5494b828f184cc8
---
 src/p2p/p2p.c | 1 +
 1 file changed, 1 insertion(+)

diff --git a/src/p2p/p2p.c b/src/p2p/p2p.c
index 738436c..fa6a849 100644
--- a/src/p2p/p2p.c
+++ b/src/p2p/p2p.c
@@ -711,6 +711,7 @@ int p2p_add_device(struct p2p_data *p2p, const u8 *addr, int freq,
 	if (os_memcmp(addr, p2p_dev_addr, ETH_ALEN) != 0)
 		os_memcpy(dev->interface_addr, addr, ETH_ALEN);
 	if (msg.ssid &&
+	    msg.ssid[1] <= sizeof(dev->oper_ssid) &&
 	    (msg.ssid[1] != P2P_WILDCARD_SSID_LEN ||
 	     os_memcmp(msg.ssid + 2, P2P_WILDCARD_SSID, P2P_WILDCARD_SSID_LEN)
 	     != 0)) {
-- 
2.7.4

