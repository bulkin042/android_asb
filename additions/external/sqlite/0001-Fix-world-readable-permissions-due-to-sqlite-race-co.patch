From 65efff03fda0c5100a20b6e5c215e6d2571a7b60 Mon Sep 17 00:00:00 2001
From: Jeff Vander Stoep <jeffv@google.com>
Date: Wed, 10 Sep 2014 13:14:28 -0700
Subject: [PATCH 1/3] Fix world-readable permissions due to sqlite race
 condition

Existing code uses umask() to temporarily modify the file permissions for
open(). A race condition can occur where a second thread reads in the
temporary value, saves it, and then restores the file to the temporary value
resulting in world-readable permissions. Backporting a known fix:
http://www.sqlite.org/src/info/6c4c2b7dba

Bug: 15288755
Change-Id: I89779f3a5ba0bec181d6614b29b1e26ea4f4f049
---
 dist/sqlite3.c | 33 +++++++++++++--------------------
 1 file changed, 13 insertions(+), 20 deletions(-)

diff --git a/dist/sqlite3.c b/dist/sqlite3.c
index 8165c95..f65fcbc 100644
--- a/dist/sqlite3.c
+++ b/dist/sqlite3.c
@@ -25426,11 +25426,7 @@ static struct unix_syscall {
                     aSyscall[13].pCurrent)
 #endif
 
-#if SQLITE_ENABLE_LOCKING_STYLE
   { "fchmod",       (sqlite3_syscall_ptr)fchmod,     0  },
-#else
-  { "fchmod",       (sqlite3_syscall_ptr)0,          0  },
-#endif
 #define osFchmod    ((int(*)(int,mode_t))aSyscall[14].pCurrent)
 
 #if defined(HAVE_POSIX_FALLOCATE) && HAVE_POSIX_FALLOCATE
@@ -25455,9 +25451,6 @@ static struct unix_syscall {
   { "fchown",       (sqlite3_syscall_ptr)fchown,          0 },
 #define osFchown    ((int(*)(int,uid_t,gid_t))aSyscall[20].pCurrent)
 
-  { "umask",        (sqlite3_syscall_ptr)umask,           0 },
-#define osUmask     ((mode_t(*)(mode_t))aSyscall[21].pCurrent)
-
 }; /* End of the overrideable system calls */
 
 /*
@@ -25561,20 +25554,20 @@ static const char *unixNextSystemCall(sqlite3_vfs *p, const char *zName){
 ** recover the hot journals.
 */
 static int robust_open(const char *z, int f, mode_t m){
-  int rc;
-  mode_t m2;
-  mode_t origM = 0;
-  if( m==0 ){
-    m2 = SQLITE_DEFAULT_FILE_PERMISSIONS;
-  }else{
-    m2 = m;
-    origM = osUmask(0);
-  }
-  do{ rc = osOpen(z,f,m2); }while( rc<0 && errno==EINTR );
-  if( m ){
-    osUmask(origM);
+  int fd;
+  mode_t m2 = m ? m : SQLITE_DEFAULT_FILE_PERMISSIONS;
+  do{
+    fd = osOpen(z,f,m2);
+  }while( fd<0 && errno==EINTR );
+  if( fd>=0 ){
+    if( m!=0 ){
+      struct stat statbuf;
+      if( osFstat(fd, &statbuf)==0 && (statbuf.st_mode&0777)!=m ){
+        osFchmod(fd, m);
+      }
+    }
   }
-  return rc;
+  return fd;
 }
 
 /*
-- 
2.7.4

