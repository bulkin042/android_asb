From 94d5fa95d1e98cedb89f4f95cf724004625baa21 Mon Sep 17 00:00:00 2001
From: Adam Vartanian <flooey@google.com>
Date: Mon, 14 Aug 2017 15:51:29 +0100
Subject: [PATCH 19/20] Fix integer overflow in utf{16,32}_to_utf8_length

Without an explicit check, the return value can wrap around and return
a value that is far too small to hold the data from the resulting
conversion.

No CTS test is provided because it would need to allocate at least
SSIZE_MAX / 2 bytes of UTF-16 data, which is unreasonable on 64-bit
devices.

Bug: 37723026
Test: run cts -p android.security
Change-Id: Ib60e738d872c1fe66997a1ccdcf186291bf37a8d
---
 libutils/Unicode.cpp | 23 ++++++++++++++++++++---
 1 file changed, 20 insertions(+), 3 deletions(-)

diff --git a/libutils/Unicode.cpp b/libutils/Unicode.cpp
index ef1057f..0c8cbf6 100644
--- a/libutils/Unicode.cpp
+++ b/libutils/Unicode.cpp
@@ -184,7 +184,15 @@ ssize_t utf32_to_utf8_length(const char32_t *src, size_t src_len)
     size_t ret = 0;
     const char32_t *end = src + src_len;
     while (src < end) {
-        ret += utf32_codepoint_utf8_length(*src++);
+        size_t char_len = utf32_codepoint_utf8_length(*src++);
+        if (SSIZE_MAX - char_len < ret) {
+            // If this happens, we would overflow the ssize_t type when
+            // returning from this function, so we cannot express how
+            // long this string is in an ssize_t.
+            android_errorWriteLog(0x534e4554, "37723026");
+            return -1;
+        }
+        ret += char_len;
     }
     return ret;
 }
@@ -419,14 +427,23 @@ ssize_t utf16_to_utf8_length(const char16_t *src, size_t src_len)
     size_t ret = 0;
     const char16_t* const end = src + src_len;
     while (src < end) {
+        size_t char_len;
         if ((*src & 0xFC00) == 0xD800 && (src + 1) < end
                 && (*(src + 1) & 0xFC00) == 0xDC00) {
             // surrogate pairs are always 4 bytes.
-            ret += 4;
+            char_len = 4;
             src += 2;
         } else {
-            ret += utf32_codepoint_utf8_length((char32_t) *src++);
+            char_len = utf32_codepoint_utf8_length((char32_t)*src++);
+        }
+        if (SSIZE_MAX - char_len < ret) {
+            // If this happens, we would overflow the ssize_t type when
+            // returning from this function, so we cannot express how
+            // long this string is in an ssize_t.
+            android_errorWriteLog(0x534e4554, "37723026");
+            return -1;
         }
+        ret += char_len;
     }
     return ret;
 }
-- 
2.7.4

